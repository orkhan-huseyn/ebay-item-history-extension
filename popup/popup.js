const fetchButton = document.getElementById('fetchButton');
const messageArea = document.getElementById('messageArea');

const events = {
  REQUEST_PURCHASE_HISTORY: 'REQUEST_PURCHASE_HISTORY',
  PURCHASE_HISTORY_ERROR: 'PURCHASE_HISTORY_ERROR',
  PURCHASE_HISTORY_SUCCESS: 'PURCHASE_HISTORY_SUCCESS',
  SIGNIN_REQUIRED: 'SIGNIN_REQUIRED',
};

fetchButton.addEventListener('click', function () {
  chrome.tabs
    .query({ active: true, currentWindow: true })
    .then((tabs) => {
      fetchButton.textContent = 'Fetching...';
      fetchButton.disabled = true;
      messageArea.hidden = true;

      chrome.tabs.sendMessage(tabs[0].id, {
        message: events.REQUEST_PURCHASE_HISTORY,
      });
    })
    .catch((error) => {
      console.log('Error querying chrome.tabs API');
      console.dir(error);
    });
});

chrome.runtime.onMessage.addListener(function (request) {
  if (request.message === events.PURCHASE_HISTORY_SUCCESS) {
    showMessage('Successfully fetched item history! 😇', 'green');
  } else if (request.message === events.PURCHASE_HISTORY_ERROR) {
    showMessage('Error fetching item history! 😨', 'red');
  } else if (request.message === events.SIGNIN_REQUIRED) {
    showMessage('Looks like you need to sign in 🤓', 'red');
  }

  fetchButton.disabled = false;
  fetchButton.textContent = 'Fetch item history';
});

function showMessage(message, color) {
  messageArea.textContent = message;
  messageArea.style.color = color;
  messageArea.hidden = false;
}
