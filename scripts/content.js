const url = new URL(location.href);
const bottomPanel = document.getElementById('BottomPanel');

const events = {
  REQUEST_PURCHASE_HISTORY: 'REQUEST_PURCHASE_HISTORY',
  PURCHASE_HISTORY_ERROR: 'PURCHASE_HISTORY_ERROR',
  PURCHASE_HISTORY_SUCCESS: 'PURCHASE_HISTORY_SUCCESS',
  SIGNIN_REQUIRED: 'SIGNIN_REQUIRED',
};

const [, , itemID] = url.pathname.split('/');

if (!itemID) {
  throw new ReferenceError('Item ID cannot be parsed from URL.');
}

chrome.runtime.onMessage.addListener(function (request) {
  if (request.message === events.REQUEST_PURCHASE_HISTORY) {
    fetch(`https://www.ebay.com/bin/purchaseHistory?item=${itemID}`)
      .then((response) => response.text())
      .then((html) => {
        const document = new DOMParser().parseFromString(html, 'text/html');
        const historyTable = document.querySelector('.app-table__table');

        if (!historyTable) {
          return Promise.reject(
            'Could not find .app-table__table inside response HTML.'
          );
        }

        const existingTable = bottomPanel.querySelector('.app-table__table');
        if (existingTable) {
          existingTable.remove();
        }

        bottomPanel.insertAdjacentElement('afterbegin', historyTable);

        chrome.runtime.sendMessage({
          message: events.PURCHASE_HISTORY_SUCCESS,
        });

        historyTable.scrollIntoView({
          block: 'center',
          behavior: 'smooth',
        });
      })
      .catch((error) => {
        if (error.message.includes('Failed to fetch')) {
          chrome.runtime.sendMessage({
            message: events.SIGNIN_REQUIRED,
          });
        } else {
          chrome.runtime.sendMessage({
            message: events.PURCHASE_HISTORY_ERROR,
          });
        }

        console.log('Error fetching purchase history.');
        console.dir(error);
      });
  }
});
